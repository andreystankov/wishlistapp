# Simple Wishlist Web App


## Description

Simple Wishlist App biuld with Symfony and Doctrine using libraries.io API

* user login/logout and registration
* the products are libraries from libraries.io
* user dashboard for wishlist items accessible only for registered users
* home page with last 10 updated PHP libraries
* search with pagination


## Getting Started

### Installing

* Clone repository
```
git clone https://bitbucket.org/andreystankov/wishlistapp.git
```
* Download dependencies
```
composer update
```
* Edit "DATABASE_URL" in .env file and configure connection to you MySql server
* Create MySql tables for the App
```
php bin/console doctrine:migrations:migrate
```
* Edit "API_LIBRARIES_IO" in .env file and add you libraries.io API key which you 
can take with login in libraries.io with your GitHub/GitLab/Bitbucket account
* Run local web server
```
php -S localhost:8000 -t public/
```

### Run with Docker Containers

* Clone repository
```
git clone https://bitbucket.org/andreystankov/wishlistapp.git
```
* Edit "API_LIBRARIES_IO" in .env file and add you libraries.io API key which you 
can take with login in libraries.io with your GitHub/GitLab/Bitbucket account
* Create Docker containers
```
docker-compose up -d --build
```
* Download dependencies
```
docker exec -it wishlist_php composer update
```
* Create MySql tables for the App
```
docker exec -it wishlist_php php bin/console doctrine:migrations:migrate
```
Now you can open http://localhost:8080 and try the App!



## Authors

Andrey Stankov
andrey.stankov@gmail.com
