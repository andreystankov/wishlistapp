<?php

namespace App\Service\Product;

interface ProductServiceInterface
{
   
    public function getLastTenUpdatedLibraries();
    public function searchLibraries($searchString, $pageNumber);
}