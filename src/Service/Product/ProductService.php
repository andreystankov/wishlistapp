<?php

namespace App\Service\Product;

use App\Service\Product\ProductServiceInterface;
use App\Repository\ProductRepository;
use App\Service\Wishlist\WishlistService;

class ProductService implements ProductServiceInterface
{


    public function __construct(ProductRepository $productRepository,
    WishlistService $wishlistService)
    {
        $this->productRepository = $productRepository;
        $this->wishlistService = $wishlistService;
    }

    public function getLastTenUpdatedLibraries()
    {
        return $this->wishlistService->apiResponceToWishlistObj($this->productRepository->lastTenUpdatedLibrariesQuery());
    }
    public function searchLibraries($searchString, $pageNumber)
    {
        return $this->wishlistService->apiResponceToWishlistObj($this->productRepository->searchLibrariesQuery($searchString, $pageNumber));
    }
}
