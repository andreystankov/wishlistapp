<?php

namespace App\Service\Wishlist;

interface WishlistServiceInterface
{
   
    public function apiResponceToWishlistObj($apiResponceArray);
    public function getWishedLibrary($platform, $searchString);
    public function getWishlistNamesArray($wishlistsArray);
}