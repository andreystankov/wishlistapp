<?php

namespace App\Service\Wishlist;

use App\Entity\Wishlist;
use App\Service\Wishlist\WishlistServiceInterface;
use App\Repository\ProductRepository;
use App\Repository\WishlistRepository;
use App\Service\Product\ProductService;
use DateTime;

use function Symfony\Component\String\s;

class WishlistService implements WishlistServiceInterface
{
    public function __construct(
        ProductRepository $productRepository,
        WishlistRepository $wishlistRepository)
    {
        $this->productRepository = $productRepository;
        $this->wishlistRepository = $wishlistRepository;
    }

    public function apiResponceToWishlistObj($apiResponceArray)
    {
        
        foreach ($apiResponceArray as $key=>$apiResponce) {
            $wishlist = new Wishlist();
            $wishlists[$key] = $wishlist->setName($apiResponce['name']);
            $wishlists[$key] = $wishlist->setDescription($apiResponce['description']);
            $wishlists[$key] = $wishlist->setRepositoryUrl($apiResponce['repository_url']);
            $wishlists[$key] = $wishlist->setLanguage($apiResponce['language']);
            $wishlists[$key] = $wishlist->setPlatform($apiResponce['platform']);
            $wishlists[$key] = $wishlist->setLatestReleaseNumber($apiResponce['latest_release_number']);
            $wishlists[$key] = $wishlist->setLatestReleasePublishedAt(new DateTime($apiResponce['latest_release_published_at']));

        }
        if(empty($wishlists)){
            return false;
        }
        return $wishlists;
    }

    public function getWishedLibrary($platform, $searchString)
    {
        return $this->apiResponceToWishlistObj(
         $this->productRepository->searchNamePlatformQuery($platform, $searchString)

        );
    }

    public function getWishlistNamesArray($wishlistsArray){
        $wishlistNamesArray = [];
        foreach($wishlistsArray as $wishlist){
            $wishlistNamesArray[$wishlist->getName()]= $wishlist;
        }
        return $wishlistNamesArray;
    }
}
