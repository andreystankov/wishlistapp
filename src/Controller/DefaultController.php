<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Product\ProductService;
use App\Service\Wishlist\WishlistService;

class DefaultController extends AbstractController
{
    public function __construct(
        ProductService $productService,
        RequestStack $requestStack,
        WishlistService $wishlistService
    ) {
        $this->productService = $productService;
        $this->requestStack = $requestStack;
        $this->wishlistService = $wishlistService;
    }
    /**
     * @Route("/", name="app_home")
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        $session = $this->requestStack->getSession();
        $wishlistNamesArray = [];
        $session->remove('searchString');
        $session->remove('pageNumber');
        if ($user) {
            $wishlistNamesArray = $this->wishlistService->getWishlistNamesArray($user->getWishlists());
        }
        $libraries = $this->productService->getLastTenUpdatedLibraries();
        return $this->render(
            'default/index.html.twig',
            [
                'libraries' => $libraries,
                'wishlistArray' => $wishlistNamesArray,
            ]
        );
    }

    /**
     * @Route("/search/{pageNumber}", name="app_search")
     */
    public function search(Request $request, $pageNumber = 1)
    {
        $user = $this->getUser();
        $session = $this->requestStack->getSession();

        if ($request->request->get('search') !== $session->get('searchString')) {
            $pageNumber = 1;
            $searchString = $request->request->get('search');
        } else {
            $searchString = $session->get('searchString');
        }
        $session->set('searchString', $searchString);
        $session->set('pageNumber', $pageNumber);
        $libraries = $this->productService->searchLibraries($searchString, $pageNumber);
        $wishlistNamesArray = $this->wishlistService->getWishlistNamesArray($user->getWishlists());
        if ($libraries === false) {
            $this->addFlash('success', 'No results!');
        }
        return $this->render(
            'default/search.html.twig',
            [
                'libraries' => $libraries,
                'wishlistArray' => $wishlistNamesArray,
            ]

        );
    }
}
