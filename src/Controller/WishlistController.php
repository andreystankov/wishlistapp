<?php

namespace App\Controller;

use App\Entity\Wishlist;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Wishlist\WishlistService;
use App\Repository\WishlistRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;

class WishlistController extends AbstractController
{

    public function __construct(
        WishlistService $wishlistService,
        WishlistRepository $wishlistRepository
    ) {
        $this->wishlistService = $wishlistService;
        $this->wishlistRepository = $wishlistRepository;
    }
    /**
     * @Route("/wishlist", name="app_wishlist")
     */
    public function wishlistList()
    {
        $user = $this->getUser();
        $wishlist = $user->getWishlists();

        return $this->render('wishlist/wishlist.html.twig', [
            'wishlists' => $wishlist,
        ]);
    }
    /**
     * @Route("/wishlistAdd/{platform}/{searchString}", name="app_wishlistAdd", methods="GET", requirements={"searchString"=".+"})
     */
    public function wishlistAdd(Request $request, $platform, $searchString)
    {
        $wishlist = $this->wishlistService->getWishedLibrary($platform, $searchString);
        $user = $this->getUser();

        $wishlist[0]->setUserId($user);
        $result = $this->wishlistRepository->write($wishlist);
        if ($result) {
            $this->addFlash('success', 'Library added to wishlist');
        }
        return $this->redirectToRoute('app_wishlist');
    }

    /**
     * @Route("/wishlistDelete/{id}", name="app_wishlistDelete")
     */
    public function wishlistDelete(Request $request, $id)
    {
        $wishlist = $this->wishlistRepository->findOneBy(['id' => $id]);
        $this->wishlistRepository->delete($wishlist);
        $this->addFlash('success', 'Library successfully deleted from wishlist');

        return $this->redirectToRoute('app_wishlist');
    }
}
