<?php

namespace App\Repository;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class ProductRepository extends ServiceEntityRepository
{
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient) {
        $this->httpClient = $httpClient;
    }

    public function lastTenUpdatedLibrariesQuery()
    {
        $response = $this->httpClient->request(
            'GET',
            'https://libraries.io/api/search?q=&languages=PHP&sort=latest_release_published_at&per_page=10&page=1'
                . '&api_key='.$_ENV['API_LIBRARIES_IO']

        );
        $response->getContent();

        return $response->toArray();
    }

    public function searchLibrariesQuery($searchString, $pageNumber)
    {
        $response = $this->httpClient->request(
            'GET',
            'https://libraries.io/api/search?q=' . $searchString . '&per_page=12&page=' . $pageNumber
                . '&' . $_ENV['API_LIBRARIES_IO']
        );
        $response->getContent();

        return $response->toArray();
    }

    public function searchNamePlatformQuery($platform, $libraryName)
    {
        $response = $this->httpClient->request(
            'GET',
            'https://libraries.io/api/' . $platform . '/' . str_replace('/', '%2F', $libraryName) . '?'
            .$_ENV['API_LIBRARIES_IO']
        );
        $response->getContent();

        return array(0 => $response->toArray());
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
