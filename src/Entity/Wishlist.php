<?php

namespace App\Entity;

use App\Repository\WishlistRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WishlistRepository::class)
 */
class Wishlist
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="wishlists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="repository_url", type="string", length=255, nullable=true)
     */
    private $repositoryUrl;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $platform;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $language;

    /**
     * @ORM\Column(name="latest_release_number", type="string", length=64, nullable=true)
     */
    private $latestReleaseNumber;

    /**
     * @ORM\Column(name="latest_release_published_at", type="datetime", nullable=true)
     */
    private $latestReleasePublishedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRepositoryUrl(): ?string
    {
        return $this->repositoryUrl;
    }

    public function setRepositoryUrl(?string $repositoryUrl): self
    {
        $this->repositoryUrl = $repositoryUrl;

        return $this;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getLatestReleaseNumber(): ?string
    {
        return $this->latestReleaseNumber;
    }

    public function setLatestReleaseNumber(?string $latestReleaseNumber): self
    {
        $this->latestReleaseNumber = $latestReleaseNumber;

        return $this;
    }

    public function getLatestReleasePublishedAt(): ?\DateTimeInterface
    {
        return $this->latestReleasePublishedAt;
    }

    public function setLatestReleasePublishedAt(?\DateTimeInterface $latestReleasePublishedAt): self
    {
        $this->latestReleasePublishedAt = $latestReleasePublishedAt;

        return $this;
    }
  
}
